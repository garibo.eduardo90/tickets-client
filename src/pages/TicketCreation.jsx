import React, { useContext, useState } from "react";
import { Col, Row, Typography, Button, Divider } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { useHideMenu } from "../hooks/useHideMenu";
import { SocketContext } from "../context/SocketContext";

export const TicketCreation = () => {
  useHideMenu(true);
  const { Title, Text } = Typography;
  const { socket } = useContext(SocketContext);
  const [ticket, setTicket] = useState(null);

  const newTicket = () => {
    socket.emit("create-ticket", null, (ticket) => {
      setTicket(ticket);
    });
  };

  return (
    <>
      <Row>
        <Col span={14} offset={6} align="center">
          <Title level={3}>Generar Ticket!</Title>
          <Button
            type="primary"
            shape="round"
            icon={<DownloadOutlined />}
            size="large"
            onClick={newTicket}
          >
            Generar Ticket
          </Button>
        </Col>
      </Row>

      {ticket ? (
        <Row style={{ marginTop: 100 }}>
          <Col span={14} offset={6} align="center">
            <Text level={2}>Su numero</Text>
            <br />
            <Text type="success" style={{ fontSize: 80 }}>
              {" "}
              {ticket.number}
            </Text>
          </Col>
        </Row>
      ) : (
        <h2>Generate a new ticket</h2>
      )}
    </>
  );
};
