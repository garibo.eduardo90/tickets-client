import React, { useContext, useState } from "react";
import { Col, Row, Typography, Button, Divider } from "antd";
import { CloseCircleOutlined, ArrowRightOutlined } from "@ant-design/icons";
import { useHideMenu } from "../hooks/useHideMenu";
import { getUserStorage } from "../helpers/getUserStorage";
import { Redirect, useHistory } from "react-router";
import { SocketContext } from "../context/SocketContext";

export const Desk = () => {
  useHideMenu(false);
  const history = useHistory();
  const [user] = useState(getUserStorage());
  const [ticket, setTicket] = useState(null);
  const { socket } = useContext(SocketContext);
  const { Title, Text } = Typography;

  const exit = () => {
    localStorage.clear();
    history.replace("/ingress");
  };
  const next = () => {
    socket.emit("next-ticket", user, (ticket) => {
      setTicket(ticket);
    });
  };

  if (!user.agente && !user.escritorio) {
    return <Redirect to="/ingress" />;
  }
  return (
    <>
      <Row>
        <Col span={20}>
          <Title level={2}>{user.agente}</Title>
          <Text>Usted esta trabajando en el escritorio </Text>
          <Text type="success">{user.escritorio}</Text>
        </Col>
        <Col span={4} align="right">
          <Button shape="round" type="danger" onClick={exit}>
            <CloseCircleOutlined />
            Salir
          </Button>
        </Col>
      </Row>
      <Divider />
      {ticket && (
        <Row>
          <Col>
            <Text>Esta atendiendo el ticket numero: </Text>
            <Text style={{ fontSize: 30 }} type="danger">
              {ticket.number}
            </Text>
          </Col>
        </Row>
      )}
      <Row>
        <Col offset={18} span={6} align="right">
          <Button onClick={next} shape="round" type="primary">
            <ArrowRightOutlined />
            Siguiente
          </Button>
        </Col>
      </Row>
    </>
  );
};
