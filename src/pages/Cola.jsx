import React, { useContext, useEffect, useState } from "react";
import { Col, Row, Typography, Button, Divider, List, Card, Tag } from "antd";
import { useHideMenu } from "../hooks/useHideMenu";
import { SocketContext } from "../context/SocketContext";

export const Cola = () => {
  const { Title, Text } = Typography;
  const [tickets, setTickets] = useState([]);
  const { socket } = useContext(SocketContext);

  useEffect(() => {
    socket.on("ticket-assigned", (data) => {
      setTickets(data);
    });

    return () => {
      socket.off("ticket-assigned");
    };
  }, [socket]);

  useEffect(() => {
    const getLatestTickets = async () => {
      const resp = await fetch("http://localhost:8080/latest");
      const data = await resp.json();
      setTickets(data.latest);
    };
    getLatestTickets();
  }, []);
  useHideMenu(true);
  return (
    <>
      <Title level={2}>Atendiendo cliente</Title>
      <Row>
        <Col span={12}>
          <List
            dataSource={tickets.slice(0, 3)}
            renderItem={(item) => (
              <List.Item>
                <Card
                  style={{ width: 400, marginTop: 16 }}
                  actions={[
                    <Tag color="volcano">{item.agente}</Tag>,
                    <Tag color="magenta">Escritorio: {item.desk}</Tag>,
                  ]}
                >
                  <Title>No. {item.number}</Title>
                </Card>
              </List.Item>
            )}
          />
        </Col>
        <Col span={12}>
          <Divider>Historial</Divider>
          <List
            dataSource={tickets.slice(4)}
            renderItem={(item) => (
              <List.Item>
                <List.Item.Meta
                  title={`Ticket No. ${item.number}`}
                  description={
                    <>
                      <Text type="secondary"> En escritorio </Text>
                      <Tag color="magenta"> {item.desk}</Tag>
                      <Text type="secondary"> Agente: </Text>
                      <Tag color="volcano"> {item.agente} </Tag>
                    </>
                  }
                />
              </List.Item>
            )}
          />
        </Col>
      </Row>
    </>
  );
};
