import React, { useContext } from "react";
import { Layout, Menu } from "antd";
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from "@ant-design/icons";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import { Ingress } from "./Ingress";
import { Cola } from "./Cola";
import { Desk } from "./Desk";
import { TicketCreation } from "./TicketCreation";
import { UIContext } from "../context/UIContext";
const { Sider, Content } = Layout;

export const RouterPage = () => {
  const { hidden } = useContext(UIContext);
  return (
    <Router>
      <Layout style={{ height: "100vh" }}>
        <Sider collapsedWidth="0" breakpoint="md" hidden={hidden}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              <Link to="/ingress">Ingresar</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              <Link to="/cola">Cola</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              <Link to="/crear-ticket">Crear Ticket</Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <Switch>
              <Route path="/ingress" component={Ingress} />
              <Route path="/cola" component={Cola} />
              <Route path="/crear-ticket" component={TicketCreation} />
              <Route path="/desk" component={Desk} />

              <Redirect to="/ingress" />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </Router>
  );
};
