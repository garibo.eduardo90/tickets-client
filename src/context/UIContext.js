import React, { createContext, useState } from "react";

export const UIContext = createContext();

export const UIProvider = ({ children }) => {
  const [hidden, setHidden] = useState(true);

  const hideMenu = () => {
    setHidden(true);
  };

  const showMenu = () => {
    setHidden(false);
  };

  return (
    <UIContext.Provider value={{ hidden, showMenu, hideMenu }}>
      {children}
    </UIContext.Provider>
  );
};
