import { useContext, useEffect } from "react";
import { UIContext } from "../context/UIContext";

export const useHideMenu = (hidde) => {
  const { hideMenu, showMenu } = useContext(UIContext);
  useEffect(() => {
    if (hidde) {
      hideMenu();
    } else {
      showMenu();
    }
  }, [hidde, hideMenu, showMenu]);
};
